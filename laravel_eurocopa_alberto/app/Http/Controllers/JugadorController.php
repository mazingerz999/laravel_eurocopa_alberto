<?php

namespace App\Http\Controllers;

use App\Models\Jugador;
use Illuminate\Http\Request;

class JugadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function buscar(Request $request)
    {
        //

        $arte = Jugador::where("numeroCamiseta", "=", $request->buscador )->pluck('nombre');

        return response()->json($arte);


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Jugadore  $jugadore
     * @return \Illuminate\Http\Response
     */
    public function show(Jugador $jugadore)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Jugadore  $jugadore
     * @return \Illuminate\Http\Response
     */
    public function edit(Jugador $jugadore)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Jugadore  $jugadore
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jugador $jugadore)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Jugadore  $jugadore
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jugador $jugadore)
    {
        //
    }
}
