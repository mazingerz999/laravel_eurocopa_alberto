<?php

namespace App\Http\Controllers;

use App\Models\Jugador;
use App\Models\Partido;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RestWebServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reset(Partido $partido)
    {
        //
        $part=Partido::where('id', '=', $partido->id)->first();

        $part->goles_pais1= 0;
        $part->goles_pais2= 0;
         return response()->json(['partido'=>"Partido {$part->id}" , "Reseteado"]);


    }
    public function posiciones(Jugador $jug)
    {
        //
        $jug=Jugador::where('pais_id', '=', $jug->pais_id)->Where('posicion', '=', $jug->posicion)->get();

         return response()->json(['jugador'=>"{$jug}"]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
        // $animal->delete();
        // return response()->json(['mensaje'=>"Se ha Borrado: Animal {$->especie}"]);
    }
}
