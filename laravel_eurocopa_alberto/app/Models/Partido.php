<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partido extends Model
{
    protected $table="partidos";
    protected $guarded=[];
    use HasFactory;


   public function paises()
   {
       # code...
       return $this->hasMany(Cuidador::class, 'pais1_id')->orWhere('pais2_id', $this->id);
   }
}
