<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table="paises";
    protected $guarded=[];
    use HasFactory;


   public function jugadores()
   {
       # code...
       return $this->hasMany(Jugador::class);
   }

   public function partidos()
   {
       # code...
       return $this->belongsTo(Partido::class, "pais1_id")->get()
       ->merge($this->belongsTo(Partido::class, "pais2_id")->get());
   }

   public function grupo()
   {
       # code...
       return $this->belongsTo(Grupo::class);

   }


   public function getRouteKeyName()
   {
       # code...
       return "slug";
   }

}
