<?php

use App\Http\Controllers\GrupoController;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\JugadorController;
use App\Http\Controllers\PaisController;
use App\Http\Controllers\RestWebServiceController;
use App\Models\Partido;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [InicioController::class, 'home'])->name('home');

Route::get('grupos', [GrupoController::class, 'index'])->name('grupos.index');

Route::get('paises/{pais}', [PaisController::class, 'show'])->name('paises.show');

Route::post('numeros/busquedaAjax', [JugadorController::class,'buscar'])->name('jugador.busquedaAjax');


// Route::get('rest', [RestWebServiceController::class, 'index']);


Route::get('api/partido/{partido}/resetear', [RestWebServiceController::class, 'reset']);
Route::get('api/jugadores/{pais}/{posicion}', [RestWebServiceController::class, 'posiciones']);
