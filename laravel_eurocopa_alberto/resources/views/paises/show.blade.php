@extends('layouts.master')

@section('titulo')
    Museo
@endsection

@section('contenido')
<br>
<br>
<br>
<br>
<br>
<div class="row">
    <div class="col-sm-9">
        <table border='1'>
        <thead>
            <tr>
                  <th>Numero</th>
                   <th>Nombre</th>
                   <th>Posicion</th>
                   <th>Edad</th>
              </tr>
         </thead>
          <tbody>
          @foreach ($pais->jugadores as $jugador)
              <tr>
                  <td>{{$jugador->numeroCamiseta}}</td>
                  <td>{{$jugador->nombre}}</td>
                  <td>{{$jugador->posicion}}</td>
                  <td>{{$jugador->fechaNacimiento}}</td>
              </tr>

          @endforeach
          </tbody>
          </table>

        </div>

       </div>

        <br>

    </div>
@endsection
