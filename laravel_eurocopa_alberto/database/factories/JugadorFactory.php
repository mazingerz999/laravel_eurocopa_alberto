<?php

namespace Database\Factories;


use App\Models\Pais;
use App\Models\Jugador;
use Illuminate\Database\Eloquent\Factories\Factory;

class JugadorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Jugador::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {



        $nombre=$this->faker->firstName('male');
        $numeroCamiseta=$this->faker->numberBetween($min = 1, $max = 25);
        $fechaNacimiento=$this->faker->dateTimeBetween( '-30 years', '+18 years');
        $posicion=$this->faker->randomElement($array = array ('POR','DEF','CEN', 'DEL'));
        $pais_Id=Pais::all()->random()->id;
        return [
            //
            'nombre'=>$nombre,
            'numeroCamiseta'=>$numeroCamiseta,
            'fechaNacimiento'=>$fechaNacimiento,
            'posicion'=>$posicion,
            'pais_id'=>$pais_Id,

        ];
    }
}
