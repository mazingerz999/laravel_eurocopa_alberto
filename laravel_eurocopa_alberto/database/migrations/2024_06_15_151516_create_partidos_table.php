<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partidos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pais1_id');
            $table->unsignedBigInteger('pais2_id');
            $table->foreign('pais1_id')->references('id')->on('paises');
            $table->foreign('pais2_id')->references('id')->on('paises');
            $table->double('goles_pais1', 4);
            $table->double('goles_pais2', 4);
            $table->double('disputado', 1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partidos');
    }
}
